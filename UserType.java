package com.company;

public class UserType extends User {
    private int status;

    UserType(){}

    UserType(String name,String surname,String userName,String password ){
        super(name, surname, userName, password);
    }

    UserType(int id,String name,String surname,String userName,String password ){
        super(id, name, surname, userName, password);
    }

    UserType(String name,String surname,String userName,String password,int type){
        super(name, surname, userName, password);
        setStatus(type);
    }

    UserType(int id,String name,String surname,String userName,String password,int type){
        super(id,name, surname, userName, password);
        setStatus(type);
    }

   public void setStatus(int type) {
        this.status = type;
    }

    public int getStatus() {
        return status;
    }


    @Override
    public String toString() {
        return super.toString()+", status: "+ status;
    }
}
