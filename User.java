package com.company;

public class User {
    private int id;
    private static int id_gen = 0;
    private String name, surname, userName;
    private String password;
    private static int last_id = 0;

    User(){
      if(last_id >id_gen){
         id_gen = Integer.valueOf(last_id);
      }
      id = id_gen;
      id_gen++;
    }

    User(String name,String surname,String userName,String password ){
      this();
      setName(name);
      setSurname(surname);
      setUserName(userName);
      setPassword(password);
    }

    User(int id,String name,String surname,String userName,String password ){
        this();
        setId(id);
        setName(name);
        setSurname(surname);
        setUserName(userName);
        setPassword(password);
        last_id = id;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setName(String x){
        this.name = x;

    }

    public void setSurname(String x){
        this.surname = x ;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String x){

            this.password = x;
    }

    public String getPassword(){
        return password;
    }

    public String getName(){
        return  name;
    }

    public String getSurname(){
        return surname;
    }

    public String getUsername(){
        return userName;
    }

    public int getId(){
        return id;
    }

    @Override
    public String toString() {
        return "id: "+id+" ,name: "+name+", surname: "+surname
                +", username: "+userName+", password:"+password;
    }
}
