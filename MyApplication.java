package com.company;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    private ArrayList<UserType> users;

    public ArrayList<UserType> getArray() {
        return users;
    }

    public MyApplication() {
        users = new ArrayList<UserType>();
    }

    public void Extract() throws FileNotFoundException {
        File file = new File("C:\\Users\\Crown\\IdeaProjects\\abc\\src\\com\\company\\db.txt");
        Scanner sc = new Scanner(file);

        int id, status;
        String password, name, surname, username;
        while (sc.hasNext()) {
            id = sc.nextInt();
            name = sc.next();
            surname = sc.next();
            username = sc.next();
            password = sc.next();
            status = sc.nextInt();

            users.add(new UserType(id, name, surname, username, password, status));
        }
    }


    public void updating() {
        try {
            FileWriter fstream1 = new FileWriter("C:\\Users\\Crown\\IdeaProjects\\abc\\src\\com\\company\\db.txt");
            BufferedWriter out1 = new BufferedWriter(fstream1);
            out1.write("");
            out1.close();
        } catch (Exception e) {
            System.err.println("Error in file cleaning: " + e.getMessage());
        }

        try (FileWriter writer = new FileWriter("C:\\Users\\Crown\\IdeaProjects\\abc\\src\\com\\company\\db.txt", false)) {

            for (int i = 0; i < users.size(); i++) {
                UserType y = users.get(i);
                String id = Integer.toString(y.getId()), status = Integer.toString(y.getStatus());
                String name, surname, username, pass;
                name = y.getName();
                surname = y.getSurname();
                username = y.getUsername();
                pass = y.getPassword();

                writer.write(id);
                writer.append(" ");
                writer.write(name);
                writer.append(" ");
                writer.write(surname);
                writer.append(" ");
                writer.write(username);
                writer.append(" ");
                writer.write(pass);
                writer.append(" ");
                writer.write(status);
                writer.append('\n');
            }

            writer.flush();
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

    public void menu() throws FileNotFoundException {
        Extract();
        int choice = 0;
        System.out.println("welcome!");
        while (choice <= 2) {
            System.out.println("1 - sign in");
            System.out.println("2 - sign up");
            System.out.println("3 - exit");
            Scanner sc = new Scanner(System.in);
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    sign_in();
                    break;
                case 2:
                    sign_up();
                    break;
                default:
                    updating();
                    break;
            }
        }
    }

    public void all_users() {
        for (int i = 0; i < users.size(); i++) {
            UserType user = users.get(i);
            System.out.println(user.getId() + " " + user.getName() + " " + user.getSurname() + " " +
                    user.getUsername() + " " + user.getPassword() + " " + user.getStatus());
        }
    }

    public void sign_in() {

        Scanner sc = new Scanner(System.in);
        System.out.println("enter the username: ");
        String username = sc.next();
        for (int i = 0; i < users.size(); i++) {
            if (username.equals(users.get(i).getUsername())) {
                System.out.println("enter the password: ");
                String pass = sc.next();
                if (pass.equals(users.get(i).getPassword())) {
                    System.out.println("Access");
                    Account(i);
                } else {
                    System.out.println("invalid password!");
                }
            }
        }
    }

    public void Account(int id) {
        UserType user = users.get(id);
        if (user.getStatus() == 1) {
            admin(id);
        } else {
            System.out.println("Welcome" + " " + user.getName() + "\n status :" + user.getStatus());
            int choice = 0;
            while (choice <= 1) {
                System.out.println("1 - change profile");
                System.out.println("2- exit");
                Scanner sc = new Scanner(System.in);
                choice = sc.nextInt();
                switch (choice) {
                    case 1:
                        change_profile(id);
                        break;
                    default:
                        break;

                }
            }
        }
    }

    private void admin(int id) {
        UserType user = users.get(id);
        System.out.println("Welcome" + " " + user.getName() + "\n status :" + user.getStatus());
        int choice = 0;
        while (choice <= 3) {
            System.out.println("1 - all users");
            System.out.println("2 - change users profile");
            System.out.println("3 - change my profile");
            System.out.println("4 - exit");
            Scanner sc = new Scanner(System.in);
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    all_users();
                    break;
                case 2:
                    change_profile(id);
                    break;
                case 3:
                    System.out.println("select one from\n" +
                            users.get(0).getId() + " to " + users.get(users.size() - 1).getId());
                    int ch = sc.nextInt();
                    change_profile(ch);
                    break;
                default:
                    break;

            }
        }
    }

    public void change_profile(int id) {
        User y = users.get(id);

        int choice = 0;

        while (choice <= 5) {
            System.out.println("What do you wnat to change?\n 1 - change password");
            System.out.println("2 - name");
            System.out.println("3 - surname");
            System.out.println("4 - username");
            System.out.println("5 - grant administrator rights");
            System.out.println("6 - exit");
            Scanner sc = new Scanner(System.in);
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    password_change(id);
                    break;
                case 2:
                    name_change(id);
                    break;
                case 3:
                    surname_change(id);
                    break;
                case 4:
                    username_change(id);
                    break;
                case 5:
                    admin_rights(id);
                    break;
                default:
                    break;
            }
        }
    }

    private void admin_rights(int id) {
        System.out.println("enter the password: ");
        Scanner sc = new Scanner(System.in);
        int code = sc.nextInt();
        if (code == 0000) {
            System.out.println("1 - turn on admin");
            System.out.println("2 - turn off admin");
            int choice = sc.nextInt();
            if (choice == 1) {
                users.get(id).setStatus(1);
            }
        } else {
            System.out.println("invalide password!");
        }
        users.get(id).setStatus(0);
    }

    public void password_change(int id) {
        User user = users.get(id);
        System.out.println("Enter the new password:\n(at least 2 digit and uppercase symbol) ");
        Scanner sc = new Scanner(System.in);
        String new_pass = sc.next();
        if (Password.check(new_pass))
            user.setPassword(new_pass);
        System.out.println("invalid password!");
    }

    public void name_change(int id) {
        User user = users.get(id);
        System.out.println("Enter the new name ");
        Scanner sc = new Scanner(System.in);
        String new_name = sc.next();
        user.setName(new_name);
    }

    public void surname_change(int id) {
        User user = users.get(id);
        System.out.println("Enter the new surname: ");
        Scanner sc = new Scanner(System.in);
        String new_sur = sc.next();
        user.setSurname(new_sur);
    }

    public void username_change(int id) {
        User user = users.get(id);
        System.out.println("Enter the new username: ");
        Scanner sc = new Scanner(System.in);
        String new_username = sc.next();
        if (chekUser(new_username))
            user.setUserName(new_username);
        System.out.println("User already exist!");

    }

    public void sign_up() {
        Scanner sc = new Scanner(System.in);
        int choice = 0,status = 0;
        String name, surname, username, pass;
        System.out.println("enter the name: ");
        name = sc.nextLine();
        System.out.println("enter the surname: ");
        surname = sc.nextLine();
        System.out.println("enter the username: ");
        username = sc.nextLine();
        if (!chekUser(username)) {
            System.out.println("this useranem already exist!");
        }
        System.out.println("create the password:\n(at least 2 digit and uppercase symbol) ");
        pass = sc.nextLine();
        if (!Password.check(pass)) {
            System.out.println("invalid password!");
           return;
        }
        UserType user = new UserType(name, surname, username, pass,status);
        users.add(user);
        System.out.println("you are:\n" +
                "1 - user\n" +
                "2 - admin");
        choice = sc.nextInt();
        if(choice == 2){
            admin_rights(users.get(users.size()-1).getId());
        }
        System.out.println("Access!");
        Account(user.getId());
    }


    private boolean chekUser(String userName) {

        for (int i = 0; i < users.size(); i++) {
            User y = users.get(i);
            if (userName == y.getUsername()) {
                return false;
            }
        }
        return true;
    }


}
