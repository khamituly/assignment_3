package com.company;

public class Password {
    private String pass;

    public static boolean check(String x){


        int count_digit = 0, countUppercase = 0;

        char c;
        for (int  i=0; i < x.length(); i++) {
            c = x.charAt(i);
            if (c >= '0' && c <= '9') {
                count_digit++;
            }
        }

        for(int i=0; i<x.length(); i++){
            if(Character.isUpperCase(x.charAt(i))){
                countUppercase++;
            }
        }

        if((count_digit >= 2 && countUppercase >= 2))
            return true;
        else return false;

    }
}
